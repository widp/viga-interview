#include <opencv2/highgui.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/sfm/fundamental.hpp>
#include <vector>
#include <iostream>

/**seems straightforward to compute **/
double data_terms(cv::Mat,cv::Mat,cv::Mat,cv::Mat);
/**This needs fundamental matrix computation**/
double geometry_terms(cv::Mat, cv::Mat,cv::Mat, cv::Mat);

double smoothness_terms();
double cost_function(double, double);

//Global constants
double K_f[9] = {5781.429394429923, 0, 1633.275461894912,
		0, 5781.429394429923, 539.8740981488083,
		0, 0, 1};
double Rs1_f[9] = { 0.9998757551737729, -0.002262136875808641, 0.01559990232124832,
		   0.002472737938320583, 0.9999058929199054, -0.01349410505127878,
		   -0.01556790874734857, 0.01353100294884545, 0.9997872534576706};

double Rs2_f[9] = {0.9976405594629493, 0.003864414327717951, -0.06854473295853769,
		  -0.004907348132621286, 0.9998746468526, -0.01505352170403514,
		  0.06847796761556796, 0.01535437668198638, 0.9975344661052814};

double Ts1_f[3] = {-0.1307652079743604,
		  0.09417434515542808,
		  0.01072325694206473};
double Ts2_f[3] = {0.4684651395620875,
		  0.09673272318954856,
		  0.1014152039305916};


int main(){
  cv::Mat f0_t,f1_t, f0_t1,f1_t1;

  f0_t = cv::imread("SceneFlowDataPreethi1/Frame0_1.png" ,cv::IMREAD_COLOR);
  f1_t = cv::imread("SceneFlowDataPreethi1/Frame0_2.png" ,cv::IMREAD_COLOR);
  f0_t1 = cv::imread("SceneFlowDataPreethi1/Frame1_1.png" ,cv::IMREAD_COLOR);
  f1_t1 =  cv::imread("SceneFlowDataPreethi1/Frame1_2.png" ,cv::IMREAD_COLOR);

 
  double data_term = data_terms(f0_t, f1_t, f0_t1, f1_t1);

  std::cout<<data_term;

  double geometry_term = geometry_terms(f0_t, f1_t, f0_t1, f1_t1);
  std::cout<<geometry_term;
  //TODO:differential energy that has to be minimized at each level of the coarse to fine
  //approach
  cv::waitKey(0);
  return 0;
}

double data_terms(cv::Mat f0_t,cv::Mat f1_t,cv::Mat f0_t1,cv::Mat f1_t1){
  double ED1 = cost_function(cv::norm(f0_t1 - f0_t), 0.001);

  double ED2 = cost_function(cv::norm(f1_t1- f1_t), 0.001);

  double ED3 = cost_function(cv::norm(f1_t - f0_t), 0.001);

  double ED4 = cost_function(cv::norm(f1_t1 - f0_t1),0.001);

  return ED1+ ED2+ ED3+ ED4;
}
double geometry_terms(cv::Mat f0_t, cv::Mat f1_t, cv::Mat f0_t1, cv::Mat f1_t1){
  //TODO: computation of EG1 and EG2
  double EG1, EG2;

  cv::Mat E,F;
  cv::Mat 
  K = cv::Mat(3,3, CV_64F, K_f),
  Rs1 = cv::Mat(3,3, CV_64F, Rs1_f),
  Rs2 = cv::Mat(3,3, CV_64F, Rs2_f),
  Ts1 = cv::Mat(3,1, CV_64F, Ts1_f),
  Ts2 = cv::Mat(3,1, CV_64F, Ts2_f);
 

  cv::sfm::essentialFromRt(Rs1,Ts1,Rs2,Ts2, E);
  cv::sfm::fundamentalFromEssential(E, K, K, F);
  std::cout<<"Printing fundamental matrix "<<F<<std::endl;
  return 0.0;
}

double cost_function(double s, double epsilon){
  double result =0.0;
  result = pow(pow(s,2) + pow(epsilon,2), 0.5);
  return result;
}
